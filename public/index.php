<?php

use Rapsody\Framework\RapsodyKernel;

require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

return function (array $context) {
    return new RapsodyKernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};
