COMPOSE = cd .docker && docker compose
RUN = $(COMPOSE) exec -e XDEBUG api

##
## Docker Compose
## --------------
##

install: build start vendor

build: ## Install and initialize the project
	@$(COMPOSE) pull
	@$(COMPOSE) build --no-cache --pull --parallel

uninstall: ## Uninstall the project
	@$(COMPOSE) down --volumes --remove-orphans

start: ## Start the project
	@$(COMPOSE) up -d

stop: ## Stop the project
	@$(COMPOSE) stop

cmd: ## Open a CLI in the application container
	$(eval xdebug ?= 0)
ifeq ($(xdebug), 1)
	@export XDEBUG=1; $(RUN) ash
else
	@$(RUN) ash
endif

.PHONY: install build uninstall start stop cmd

##
## Tools
## -----
##

ci: quality tests ## Check CI results

vendor: ## Install the project dependencies
	@$(RUN) composer install

.PHONY: ci vendor

##
## Quality
## -------
##

quality: php-cs-fixer phpstan ## Run the quality tools

php-cs-fixer: ## Run PHP Coding Standards Fixer
	$(eval dry-run ?= 1)
ifeq ($(dry-run), 1)
	@$(RUN) vendor/bin/php-cs-fixer fix --dry-run
else
	@$(RUN) vendor/bin/php-cs-fixer fix
endif

phpstan: ## Run PHP Static Analysis Tool
	@$(RUN) vendor/bin/phpstan analyze

.PHONY: quality php-cs-fixer phpstan

##
## Tests
## -----
##

tests: phpunit behat ## Run the project tests

phpunit: ## Run PHPUnit
	$(eval coverage ?= 0)
ifeq ($(coverage), 1)
	@export XDEBUG=1; $(RUN) vendor/bin/phpunit --coverage-text --coverage-html=var/report/
else
	@$(RUN) vendor/bin/phpunit
endif

behat: ## Run Behat
	@$(RUN) vendor/bin/behat

.PHONY: tests phpunit behat


##


help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

.PHONY: help
.DEFAULT_GOAL:= help
