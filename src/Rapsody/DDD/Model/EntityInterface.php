<?php

namespace Rapsody\DDD\Model;

interface EntityInterface
{
    public function getId(): string|int|float;
}
