<?php

namespace Rapsody\DDD\Model;

use Doctrine\ORM\Mapping as ORM;

trait EntityTrait
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', unique: true)]
    protected string $id;
}
