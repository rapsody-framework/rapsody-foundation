<?php

namespace Rapsody\Exception;

use Rapsody\Framework\RapsodyKernel;

final class KernelAlreadyBootedException extends LogicException
{
    public function __construct(
        private readonly RapsodyKernel $kernel,
        string $message = '',
        ?\Throwable $previous = null,
        int $code = 0,
    ) {
        parent::__construct($message, $previous, $code);
    }

    public static function withKernel(RapsodyKernel $kernel, ?\Throwable $previous = null, int $code = 0): self
    {
        return new self($kernel, 'Kernel already booted', $previous, $code);
    }

    public function getKernel(): RapsodyKernel
    {
        return $this->kernel;
    }
}
