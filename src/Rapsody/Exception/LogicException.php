<?php

namespace Rapsody\Exception;

class LogicException extends \LogicException
{
    public function __construct(string $message = '', ?\Throwable $previous = null, int $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}
