<?php

namespace Rapsody\API\ArgumentResolver;

use Rapsody\API\Serializer\SerializerInterface;
use Rapsody\API\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

final class ResourceResolver implements ArgumentValueResolverInterface
{
    public function __construct(
        private SerializerInterface $serializer,
        private ValidatorInterface $validator,
    ) {}

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return
            !$request->isMethodSafe()
            && !empty($request->getContent())
            && \class_exists($argument->getType())
        ;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $resource = $this->serializer->deserialize($request, $argument->getType());
        $this->validator->validate($resource);

        yield $resource;
    }
}
