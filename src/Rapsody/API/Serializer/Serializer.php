<?php

namespace Rapsody\API\Serializer;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

class Serializer implements SerializerInterface
{
    private const FORMAT = 'json';

    private array $defaultContext;

    public function __construct(
        private \Symfony\Component\Serializer\SerializerInterface $serializer
    ) {
        $this->defaultContext = [
            DateTimeNormalizer::FORMAT_KEY => 'U',
        ];
    }

    public function serialize(object|array $resource): string
    {
        $context = \array_merge($this->defaultContext, [
            'groups' => [\is_array($resource) ? 'get_collection' : 'get_item'],
        ]);

        return $this->serializer->serialize($resource, self::FORMAT, $context);
    }

    public function deserialize(Request $request, string $resourceClass): object|array
    {
        if (!\class_exists($resourceClass)) {
            throw new \InvalidArgumentException('Argument $resourceFqcn must be a fully qualified class name');
        }

        $context = \array_merge($this->defaultContext, [
            'groups' => [\strtolower($request->getMethod())],
        ]);

        return $this->serializer->deserialize((string) $request->getContent(), $resourceClass, self::FORMAT, $context);
    }
}
