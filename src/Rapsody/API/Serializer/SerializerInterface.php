<?php

namespace Rapsody\API\Serializer;

use Symfony\Component\HttpFoundation\Request;

interface SerializerInterface
{
    public function serialize(object|array $resource): string;

    public function deserialize(Request $request, string $resourceClass): object|array;
}
