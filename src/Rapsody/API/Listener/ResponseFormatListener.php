<?php

namespace Rapsody\API\Listener;

use Rapsody\API\Exception\UnsupportedFormatException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class ResponseFormatListener
{
    public function __construct(
        private array $supportedContentTypes = [],
    ) {}

    public function __invoke(ViewEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        if ($event->getControllerResult() instanceof Response) {
            return;
        }

        $acceptType = $event->getRequest()->headers->get('Accept');
        if (null === $acceptType) {
            throw new UnsupportedFormatException('HTTP header "Accept" is missing');
        }

        $acceptType = \strtolower($acceptType);
        if (!\in_array($acceptType, \array_keys($this->supportedContentTypes))) {
            throw new UnsupportedFormatException(\sprintf('Content-Type "%s" is not supported. Please refer to the API documentation for supported formats.', $contentType));
        }

        $event->getRequest()->attributes->set('serialization_formation', $this->supportedContentTypes[$acceptType]);
    }
}
