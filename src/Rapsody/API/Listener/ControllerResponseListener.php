<?php

namespace Rapsody\API\Listener;

use Rapsody\API\Exception\UnsupportedFormatException;
use Rapsody\API\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class ControllerResponseListener
{
    public function __construct(
        private SerializerInterface $serializer,
    ) {}

    public function __invoke(ViewEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        if ($event->getControllerResult() instanceof Response) {
            return;
        }

        if (null === $event->getControllerResult()) {
            $response = new Response(null, Response::HTTP_NO_CONTENT);
        } else {
            $response = JsonResponse::fromJsonString($this->serializer->serialize($event->getControllerResult()));
        }

        $event->setResponse($response);
    }
}
