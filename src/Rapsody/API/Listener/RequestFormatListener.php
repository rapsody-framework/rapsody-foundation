<?php

namespace Rapsody\API\Listener;

use Rapsody\API\Controller\RESTController;
use Rapsody\API\Exception\UnsupportedFormatException;
use Symfony\Component\HttpKernel\Event\ControllerEvent;

class RequestFormatListener
{
    public function __construct(
        private array $supportedContentTypes = [],
    ) {}

    public function __invoke(ControllerEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        if (!$event->getController() instanceof RESTController) {
            return;
        }

        $contentType = $event->getRequest()->headers->get('Content-Type');
        if (null === $contentType) {
            throw new UnsupportedFormatException('HTTP header "Content-Type" is missing');
        }

        $contentType = \strtolower($contentType);
        if (!\in_array($contentType, \array_keys($this->supportedContentTypes))) {
            throw new UnsupportedFormatException(\sprintf('Content-Type "%s" is not supported. Please refer to the API documentation for supported formats.', $contentType));
        }

        $event->getRequest()->attributes->set('deserialization_formation', $this->supportedContentTypes[$contentType]);
    }
}
