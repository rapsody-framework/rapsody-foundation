<?php

namespace Rapsody\API\Validator;

use Rapsody\API\Exception\ValidationException;

interface ValidatorInterface
{
    /**
     * @throws ValidationException
     */
    public function validate(object|array $resource): void;
}
