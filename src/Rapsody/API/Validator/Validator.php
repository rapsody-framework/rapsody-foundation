<?php

namespace Rapsody\API\Validator;

use Rapsody\API\Exception\ValidationException;

class Validator implements ValidatorInterface
{
    public function __construct(
        private \Symfony\Component\Validator\Validator\ValidatorInterface $validator,
    ) {}

    /**
     * @throws ValidationException
     */
    public function validate(object|array $resource): void
    {
        $violations = $this->validator->validate($resource);

        if (0 < $violations->count()) {
            throw ValidationException::fromConstraintViolations($violations);
        }
    }
}
