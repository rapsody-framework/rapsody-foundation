<?php

namespace Rapsody\API\Security;

use Symfony\Component\Security\Core\User\UserInterface;

final class User implements UserInterface
{
    private const ROLE_USER = 'ROLE_USER';

    public function __construct(
        private string $id,
    ) {}

    public function getUserIdentifier(): string
    {
        return $this->id;
    }

    public function getRoles(): array
    {
        return [self::ROLE_USER];
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
