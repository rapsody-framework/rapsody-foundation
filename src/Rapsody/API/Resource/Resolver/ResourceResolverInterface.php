<?php

namespace Rapsody\API\Resource\Resolver;

use Rapsody\API\Model\ResourceInterface;
use Symfony\Component\HttpFoundation\Request;

interface ResourceResolverInterface
{
    public function resolve(Request $request): iterable|ResourceInterface;
}
