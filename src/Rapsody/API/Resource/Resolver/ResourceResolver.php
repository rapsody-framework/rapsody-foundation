<?php

namespace Rapsody\API\Resource\Resolver;

use Rapsody\API\Model\ResourceInterface;
use Rapsody\API\Serializer\SerializerInterface;
use Rapsody\API\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class ResourceResolver implements ResourceResolverInterface
{
    public function __construct(
        private RouterInterface $router,
        private SerializerInterface $serializer,
        private ValidatorInterface $validator,
    ) {}

    public function resolve(Request $request): iterable|ResourceInterface
    {
        $route = $this->router->getRouteCollection()->get($request->attributes->get('_route'));
        $resourceFqcn = $route->getOption('resource');

        if (
            null === $resourceFqcn
            || empty($request->getContent())
            || !\class_exists($resourceFqcn)
            || (0 >= \count((new \ReflectionClass($resourceFqcn))->getAttributes(ResourceInterface::class)))
        ) {
            throw new \Exception();
        }

        $resource = $this->serializer->deserialize($request, $resourceFqcn);
        $this->validator->validate($resource);

        return $resource;
    }
}
