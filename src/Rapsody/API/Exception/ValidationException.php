<?php

namespace Rapsody\API\Exception;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends \Exception
{
    public function __construct(
        private array $errors = [],
        string $message = 'invalid_body',
        \Throwable $previous = null,
        int $code = 0
    ) {
        parent::__construct($message, $previous, $code);
    }

    public static function fromConstraintViolations(ConstraintViolationListInterface $violations, string $message = 'invalid_body', ?\Throwable $previous = null, array $headers = [], int $code = 0): self
    {
        $errors = [];
        /** @var ConstraintViolationInterface $violation */
        foreach ($violations as $violation) {
            $errors[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        return new self($errors, $message, $previous, $code);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
