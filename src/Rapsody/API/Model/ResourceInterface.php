<?php

namespace Rapsody\API\Model;

interface ResourceInterface
{
    public function getCommands(): iterable;

    public function getQueries(): iterable;
}
