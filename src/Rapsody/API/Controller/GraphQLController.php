<?php

namespace Rapsody\API\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class GraphQLController
{
    public function get(Request $request): Response
    {
        $query = $request->query->get('query');
        if (null === $query) {
            throw new \Exception('GraphQL query is missing');
        }

        return new Response('OK');
    }
}
