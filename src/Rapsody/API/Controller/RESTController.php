<?php

namespace Rapsody\API\Controller;

use Boulzy\CQRS\CommandBusInterface;
use Boulzy\CQRS\QueryBusInterface;
use Rapsody\API\Model\ResourceInterface;
use Rapsody\API\Resource\Resolver\ResourceResolverInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class RESTController
{
    public function __construct(
        private ResourceResolverInterface $resourceResolver,
        private CommandBusInterface $commandBus,
        private QueryBusInterface $queryBus,
    ) {}

    public function __invoke(Request $request): Response
    {
        if (!empty($body = $request->getContent())) {
            $resources = $this->resourceResolver->resolve($request);
            if (!\is_array($resources)) {
                $resources = [$resources];
            }

            $results = [];
            /** @var ResourceInterface $resource */
            foreach ($resources as $resource) {
                $commands = $resource->getCommands();
                foreach ($commands as $command) {
                    $this->commandBus->dispatch($command);
                }
            }

            return new JsonResponse();
        }

        
    }
}
