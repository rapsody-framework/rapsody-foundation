<?php

namespace Oikonomos\Domain\ValueObject;

final class Tag
{
    private const TAG_REGEXP = '/^[a-z0-9\-_ ]{2,16}$/';

    public static function fromString(string $tag): Tag
    {
        $tag = \strtolower(\trim($tag));

        if (!\preg_match(self::TAG_REGEXP, $tag)) {
            throw new \InvalidArgumentException(
                'A tag can only contain alphanumerics, spaces, dashed and underscores,
                and must be between two and sixteen characters long'
            );
        }

        return new self($tag);
    }

    public function equals($tag): bool
    {
        return $tag instanceof Tag && $tag->tag === $this->tag;
    }

    private function __construct(
        public readonly string $tag,
    ) {}

    public function __toString(): string
    {
        return $this->tag;
    }
}
