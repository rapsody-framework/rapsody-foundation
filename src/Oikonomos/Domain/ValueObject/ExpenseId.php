<?php

namespace Oikonomos\Domain\ValueObject;

final class ExpenseId
{
    use IdTrait;
}
