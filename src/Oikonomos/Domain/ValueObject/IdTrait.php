<?php

namespace Oikonomos\Domain\ValueObject;

use Doctrine\ORM\Mapping as ORM;

trait IdTrait
{
    public static function fromString(string $id): self
    {
        if (0 === \strlen($id)) {
            throw new \InvalidArgumentException('An identifier cannot be empty');
        }

        return new self($id);
    }

    private function __construct(
        #[ORM\Column(type: 'string')]
        public readonly string $id,
    ) {}

    public function equals($id): bool
    {
        $currentClass = \get_class();

        return $id instanceof $currentClass && $id->id === $this->id;
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
