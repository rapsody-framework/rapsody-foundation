<?php

namespace Oikonomos\Domain\ValueObject;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
final class Label
{
    public static function fromString(string $label): Label
    {
        return new self($label);
    }

    private function __construct(
        #[ORM\Column(type: 'string')]
        public readonly string $label,
    ) {
        if (0 === \strlen($label)) {
            throw new \InvalidArgumentException('A label cannot be empty');
        }
    }

    public function __toString(): string
    {
        return $this->label;
    }
}
