<?php

namespace Oikonomos\Domain\ValueObject;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
final class AccountName
{
    public static function fromString(string $name): AccountName
    {
        return new self($name);
    }

    private function __construct(
        #[ORM\Column(type: 'string')]
        public readonly string $name,
    ) {
        if (0 === \strlen($name)) {
            throw new \InvalidArgumentException('An account name cannot be empty');
        }
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
