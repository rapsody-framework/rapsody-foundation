<?php

namespace Oikonomos\Domain\ValueObject;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
final class AccountId
{
    use IdTrait;
}
