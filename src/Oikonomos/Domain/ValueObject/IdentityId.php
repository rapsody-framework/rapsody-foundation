<?php

namespace Oikonomos\Domain\ValueObject;

use Doctrine\ORM\Mapping as ORM;

final class IdentityId
{
    public static function fromString(string $identityId): IdentityId
    {
        return new self($identityId);
    }

    private function __construct(
        public readonly string $identityId,
    ) {}

    public function __toString(): string
    {
        return $this->identityId;
    }
}
