<?php

namespace Oikonomos\Domain\ValueObject;

final class TransactionId
{
    use IdTrait;
}