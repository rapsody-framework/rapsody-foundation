<?php

namespace Oikonomos\Domain\ValueObject;

enum AccountStatus: string
{
    case OPENED = 'OPENED';
    case CLOSED = 'CLOSED';
}
