<?php

namespace Oikonomos\Domain\ValueObject;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
final class UserId
{
    use IdTrait;
}
