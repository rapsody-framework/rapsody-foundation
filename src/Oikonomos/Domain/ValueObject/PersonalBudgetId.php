<?php

namespace Oikonomos\Domain\ValueObject;

final class PersonalBudgetId
{
    use IdTrait;
}
