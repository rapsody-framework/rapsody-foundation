<?php

namespace Oikonomos\Domain\Event;

use Money\Money;
use Oikonomos\Domain\ValueObject\AccountId;

final class TransactionCreated
{
    public function __construct(
        public readonly AccountId $accountId,
        public readonly Money $amount,
    ) {}
}
