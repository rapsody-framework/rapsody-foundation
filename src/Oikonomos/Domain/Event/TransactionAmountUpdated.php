<?php

namespace Oikonomos\Domain\Event;

use Money\Money;
use Oikonomos\Domain\ValueObject\AccountId;

final class TransactionAmountUpdated
{
    public function __construct(
        public readonly AccountId $accountId,
        public readonly Money $oldAmount,
        public readonly Money $newAmount,
    ) {}
}
