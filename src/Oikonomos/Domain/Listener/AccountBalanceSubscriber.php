<?php

namespace Oikonomos\Domain\Listener;

use Boulzy\StaticEventDispatcher\SubscriberInterface;
use Oikonomos\Domain\Event\TransactionAmountUpdated;
use Oikonomos\Domain\Event\TransactionCreated;
use Oikonomos\Domain\Event\TransactionDeleted;
use Oikonomos\Domain\Repository\AccountRepositoryInterface;

final class AccountBalanceSubscriber implements SubscriberInterface
{
    public function __construct(
        private AccountRepositoryInterface $accountRepository,
    ) {}

    public function addTransactionAmount(TransactionCreated $event): void
    {
        $account = $this->accountRepository->get($event->accountId);

        $account->updateBalance($account->getBalance()->add($event->amount));
    }

    public function updateTransactionAmount(TransactionAmountUpdated $event): void
    {
        $account = $this->accountRepository->get($event->accountId);

        $account->updateBalance($account->getBalance()->subtract($event->oldAmount)->add($event->newAmount));
    }

    public function subtractTransactionAmount(TransactionDeleted $event): void
    {
        $account = $this->accountRepository->get($event->accountId);

        $account->updateBalance($account->getBalance()->subtract($event->amount));
    }

    public function getSubscribedEvents(): array
    {
        return [
            TransactionCreated::class => 'addTransactionAmount',
            TransactionAmountUpdated::class => 'updateTransactionAmount',
            TransactionDeleted::class => 'subtractTransactionAmount',
        ];
    }
}
