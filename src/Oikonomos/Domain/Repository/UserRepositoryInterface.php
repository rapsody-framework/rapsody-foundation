<?php

namespace Oikonomos\Domain\Repository;

use Oikonomos\Domain\Model\User;
use Oikonomos\Domain\ValueObject\UserId;

interface UserRepositoryInterface
{
    public function add(User $user): void;
}
