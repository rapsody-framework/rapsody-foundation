<?php

namespace Oikonomos\Domain\Repository;

use Oikonomos\Domain\Model\Transaction;
use Oikonomos\Domain\ValueObject\TransactionId;

interface TransactionRepositoryInterface
{
    public function add(Transaction $transaction): void;
}