<?php

namespace Oikonomos\Domain\Repository;

use Oikonomos\Domain\Model\Account;
use Oikonomos\Domain\ValueObject\AccountId;
use Oikonomos\Domain\ValueObject\UserId;

interface AccountRepositoryInterface
{
    public function add(Account $account): void;

    public function get(AccountId $id): Account;

    public function findByUser(UserId $userId, ?int $limit = null, ?int $offset = null, array $statuses = []): iterable;
}