<?php

namespace Oikonomos\Domain\Repository;

use Oikonomos\Domain\Model\PersonalBudget;
use Oikonomos\Domain\ValueObject\PersonalBudgetId;

interface PersonalBudgetRepositoryInterface
{
    public function add(PersonalBudget $personalBudget): void;

    public function get(PersonalBudgetId $id): PersonalBudget;
}