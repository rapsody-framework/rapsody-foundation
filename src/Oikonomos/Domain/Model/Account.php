<?php

namespace Oikonomos\Domain\Model;

use Doctrine\ORM\Mapping as ORM;
use Money\Currency;
use Money\Money;
use Oikonomos\Domain\ValueObject\AccountId;
use Oikonomos\Domain\ValueObject\AccountName;
use Oikonomos\Domain\ValueObject\AccountStatus;
use Oikonomos\Domain\ValueObject\UserId;
use Rapsody\DDD\Model\EntityTrait;

#[ORM\Entity]
final class Account
{
    use EntityTrait;

    private const DEFAULT_CURRENCY = 'EUR';

    public static function create(AccountId $id, UserId $userId, AccountName $name, ?Money $balance = null): Account
    {
        $balance ??= new Money(0, new Currency(self::DEFAULT_CURRENCY));

        return new self($id, $userId, $name, $balance, AccountStatus::OPENED);
    }

    public function rename(AccountName $name): void
    {
        $this->name = $name;
    }

    public function updateBalance(Money $balance): void
    {
        $this->balance = $balance;
    }

    public function close(bool $force = false): void
    {
        if (!$this->balance->isZero() && !$force) {
            throw new \RuntimeException('An account balance must be 0 for it to be closed');
        }

        $this->status = AccountStatus::CLOSED;
    }

    public function __construct(
        AccountId $id,
        #[ORM\Embedded(class: UserId::class, columnPrefix: 'user')]
        private UserId $userId,
        #[ORM\Embedded(class: AccountName::class, columnPrefix: false)]
        private AccountName $name,
        #[ORM\Embedded(class: Money::class)]
        private Money $balance,
        #[ORM\Column(type: 'string', enumType: AccountStatus::class)]
        private AccountStatus $status,
    ) {
        $this->id = (string) $id;
    }

    public function getId(): AccountId
    {
        return AccountId::fromString($this->id);
    }

    public function getUserId(): UserId
    {
        return $this->userId;
    }

    public function getName(): AccountName
    {
        return $this->name;
    }

    public function getBalance(): Money
    {
        return $this->balance;
    }

    public function isClosed(): bool
    {
        return AccountStatus::CLOSED === $this->status;
    }
}
