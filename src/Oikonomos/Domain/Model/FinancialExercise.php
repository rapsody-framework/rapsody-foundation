<?php

namespace Oikonomos\Domain\Model;

use Oikonomos\Domain\ValueObject\FinancialExercisePeriod;

final class FinancialExercise
{
    public static function generate(PersonalBudget $personalBudget, ?\DateTimeImmutable $startAt): FinancialExercise
    {
        $startAt ??= new \DateTimeImmutable('midnight first day of this month');

//        switch ($period) {
//            case FinancialExercisePeriod::YEARLY:
//                $startAt = new \DateTimeImmutable('midnight first day of January '.$startAt->format('Y'));
//                $endAt = $startAt->add(new \DateInterval('P1Y'));
//                break;
//            case FinancialExercisePeriod::MONTHLY:
//                $startAt = new \DateTimeImmutable('midnight first day of '.$startAt->format('F Y'));
//                $endAt = $startAt->add(new \DateInterval('P1M'));
//                break;
//        }

        return new self;
    }
}
