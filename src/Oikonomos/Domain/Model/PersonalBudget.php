<?php

namespace Oikonomos\Domain\Model;

use Doctrine\ORM\Mapping as ORM;
use Money\Currency;
use Money\Money;
use Oikonomos\Domain\ValueObject\ExpenseId;
use Oikonomos\Domain\ValueObject\IncomeId;
use Oikonomos\Domain\ValueObject\Label;
use Oikonomos\Domain\ValueObject\PersonalBudgetId;
use Oikonomos\Domain\ValueObject\UserId;

#[ORM\Entity]
final class PersonalBudget
{
    private const DEFAULT_CURRENCY = 'EUR';

    public static function create(PersonalBudgetId $id, UserId $userId): PersonalBudget
    {
        return new self($id, $userId, new Money(0, new Currency(self::DEFAULT_CURRENCY)));
    }

    public function addIncome(IncomeId $incomeId, Label $label, Money $amount): void
    {
        $this->incomes[] = new Income($incomeId, $label, $amount);

        $this->calculateBalance();
    }

    public function addExpense(ExpenseId $expenseId, Label $label, Money $amount): void
    {
        $this->expenses[] = new Expense($expenseId, $label, $amount);

        $this->calculateBalance();
    }

    public function updateIncomeLabel(IncomeId $incomeId, Label $label): void
    {
        foreach ($this->incomes as $income) {
            if (!$income->getId()->equals($incomeId)) {
                continue;
            }

            $income->updateLabel($label);
        }
    }

    public function updateExpenseLabel(ExpenseId $expenseId, Label $label): void
    {
        foreach ($this->expenses as $expense) {
            if (!$expense->getId()->equals($expenseId)) {
                continue;
            }

            $expense->updateLabel($label);
        }
    }

    public function updateIncomeAmount(IncomeId $incomeId, Money $amount): void
    {
        foreach ($this->incomes as $income) {
            if (!$income->getId()->equals($incomeId)) {
                continue;
            }

            $income->updateAmount($amount);
        }

        $this->calculateBalance();
    }

    public function updateExpanseAmount(ExpenseId $expenseId, Money $amount): void
    {
        foreach ($this->expenses as $expense) {
            if (!$expense->getId()->equals($expenseId)) {
                continue;
            }

            $expense->updateAmount($amount);
        }

        $this->calculateBalance();
    }

    public function deleteIncome(IncomeId $incomeId)
    {
        $incomes = $this->incomes;

        foreach ($incomes as $key => $income) {
            if (!$income->getId()->equals($incomeId)) {
                continue;
            }

            unset($incomes[$key]);
        }

        $this->incomes = \array_values($incomes);

        $this->calculateBalance();
    }

    public function deleteExpense(ExpenseId $expenseId)
    {
        $expenses = $this->expenses;

        foreach ($expenses as $key => $expense) {
            if (!$expense->getId()->equals($expenseId)) {
                continue;
            }

            unset($expenses[$key]);
        }

        $this->expenses = \array_values($expenses);

        $this->calculateBalance();
    }

    public function calculateBalance(): void
    {
        $balance = new Money(0, new Currency(self::DEFAULT_CURRENCY));

        foreach ($this->incomes as $income) {
            $balance = $balance->add($income->getAmount());
        }

        foreach ($this->expenses as $expense) {
            $balance = $balance->subtract($expense->getAmount());
        }

        $this->balance = $balance;
    }

    #[ORM\Id]
    #[ORM\Column(type: 'string')]
    private string $id;

    public function __construct(
        PersonalBudgetId $id,
        private UserId $userId,
        private Money $balance,
        private array $incomes = [],
        private array $expenses = [],
    ) {
        $this->id = (string) $id;
    }

    public function getId(): PersonalBudgetId
    {
        return PersonalBudgetId::fromString($this->id);
    }

    public function getUserId(): UserId
    {
        return $this->userId;
    }

    public function getBalance(): Money
    {
        return $this->balance;
    }

    public function getIncomes(): array
    {
        return $this->incomes;
    }

    public function getExpenses(): array
    {
        return $this->expenses;
    }
}