<?php

namespace Oikonomos\Domain\Model;

use Boulzy\StaticEventDispatcher\EventDispatcher;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Money\Money;
use Oikonomos\Domain\Event\TransactionAmountUpdated;
use Oikonomos\Domain\Event\TransactionCreated;
use Oikonomos\Domain\ValueObject\AccountId;
use Oikonomos\Domain\ValueObject\Label;
use Oikonomos\Domain\ValueObject\Tag;
use Oikonomos\Domain\ValueObject\TransactionId;
use Oikonomos\Domain\ValueObject\UserId;
use Rapsody\DDD\Model\EntityTrait;

#[ORM\Entity]
final class Transaction
{
    use EntityTrait;

    public static function create(TransactionId $id, UserId $userId, AccountId $accountId, Label $label, Money $amount, ?\DateTimeImmutable $date = null, array $tags = []): Transaction
    {
        $transaction = new self($id, $userId, $accountId, $label, $amount, $date ?? new \DateTimeImmutable(), $tags);
        $transaction->setTags($tags);

        EventDispatcher::dispatch(new TransactionCreated($transaction->getAccountId(), $transaction->getAmount()));

        return $transaction;
    }

    public function tag(Tag $tag): void
    {
        if (\in_array((string) $tag, $this->tags)) {
            return;
        }

        $this->tags[] = (string) $tag;
    }

    public function untag(Tag $tag): void
    {
        if (false === $key = \array_search((string) $tag, $this->tags)) {
            return;
        }

        \array_splice($this->tags, $key, 1);
    }

    public function updateAmount(Money $amount): void
    {
        $oldAmount = $this->amount;

        $this->amount = $amount;

        EventDispatcher::dispatch(new TransactionAmountUpdated($this->accountId, $oldAmount, $this->amount));
    }

    #[ORM\Column(type: 'json')]
    private array $tags;

    public function __construct(
        TransactionId $id,
        #[ORM\Embedded(class: UserId::class, columnPrefix: 'user')]
        private UserId $userId,
        #[ORM\Embedded(class: AccountId::class, columnPrefix: 'account')]
        private AccountId $accountId,
        #[ORM\Embedded(class: Label::class, columnPrefix: false)]
        private Label $label,
        #[ORM\Embedded(class: Money::class, columnPrefix: 'transaction')]
        private Money $amount,
        #[ORM\Column(type: 'datetime_immutable')]
        private \DateTimeImmutable $date,
        array $tags,
    ) {
        $this->id = (string) $id;

        $this->setTags($tags);
    }

    public function getId(): TransactionId
    {
        return TransactionId::fromString($this->id);
    }

    public function getUserId(): UserId
    {
        return $this->userId;
    }

    public function getAccountId(): AccountId
    {
        return $this->accountId;
    }

    public function getLabel(): Label
    {
        return $this->label;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function getTags(): array
    {
        return \array_map(fn(string $tag) => Tag::fromString($tag), $this->tags);
    }

    public function setTags(array $tags): void
    {
        $this->tags = \array_map(fn(Tag $tag) => (string) $tag, $tags);
    }
}