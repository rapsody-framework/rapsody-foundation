<?php

namespace Oikonomos\Domain\Model;

use Money\Money;
use Oikonomos\Domain\ValueObject\IncomeId;
use Oikonomos\Domain\ValueObject\Label;
use RRule\RRule;

final class Income
{
    public function updateLabel(Label $label): void
    {
        $this->label = $label;
    }

    public function updateAmount(Money $amount): void
    {
        if ($amount->isNegative() || $amount->isZero()) {
            throw new \InvalidArgumentException('An income amount must be positive');
        }

        $this->amount = $amount;
    }

    private string $recurrence;

    public function __construct(
        private IncomeId $id,
        private Label $label,
        private Money $amount,
        RRule $recurrence,
    ) {
        if ($amount->isNegative() || $amount->isZero()) {
            throw new \InvalidArgumentException('An income amount must be positive');
        }

        $this->recurrence = $recurrence->rfcString();
    }

    public function getId(): IncomeId
    {
        return $this->id;
    }

    public function getLabel(): Label
    {
        return $this->label;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }

    public function getRecurrence(): RRule
    {
        return RRule::createFromRfcString($this->recurrence);
    }
}
