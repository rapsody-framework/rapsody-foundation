<?php

namespace Oikonomos\Domain\Model;

use Doctrine\ORM\Mapping as ORM;
use Oikonomos\Domain\ValueObject\IdentityId;
use Oikonomos\Domain\ValueObject\UserId;
use Rapsody\DDD\Model\EntityTrait;

#[ORM\Entity]
#[ORM\Table(name: '`User`')]
final class User
{
    public static function create(UserId $id, IdentityId $identityId): User
    {
        return new self($id);
    }

    public function __construct(
        #[ORM\Id]
        #[ORM\Column(type: 'stringable', unique: true, options: ['class' => UserId::class])]
        private UserId $id,
        #[ORM\Column(type: 'stringable', unique: true, options: ['class' => IdentityId::class])]
        private IdentityId $identityId,
    ) {}

    public function getId(): UserId
    {
        return UserId::fromString($this->id);
    }
}
