<?php

namespace Oikonomos\Domain\Model;

use Money\Money;
use Oikonomos\Domain\ValueObject\ExpenseId;
use Oikonomos\Domain\ValueObject\Label;

final class Expense
{
    public function updateLabel(Label $label): void
    {
        $this->label = $label;
    }

    public function updateAmount(Money $amount): void
    {
        if ($amount->isNegative() || $amount->isZero()) {
            throw new \InvalidArgumentException('An expense amount must be positive');
        }

        $this->amount = $amount;
    }

    public function __construct(
        private ExpenseId $id,
        private Label $label,
        private Money $amount
    ) {
        if ($amount->isNegative() || $amount->isZero()) {
            throw new \InvalidArgumentException('An expense amount must be positive');
        }
    }

    public function getId(): ExpenseId
    {
        return $this->id;
    }

    public function getLabel(): Label
    {
        return $this->label;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }
}
