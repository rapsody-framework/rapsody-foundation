<?php

namespace Oikonomos\Application\FinancialExercise\Generate;

use Boulzy\CQRS\CommandInterface;

final class GenerateFinancialExerciseCommand implements CommandInterface
{
    public function __construct(
        public readonly string $personalBudgetId,
    ) {}
}
