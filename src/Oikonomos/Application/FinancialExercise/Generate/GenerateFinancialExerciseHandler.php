<?php

namespace Oikonomos\Application\FinancialExercise\Generate;

use Boulzy\CQRS\CommandHandlerInterface;
use Oikonomos\Application\FinancialExercise\FinancialExercise;
use Oikonomos\Domain\Repository\PersonalBudgetRepositoryInterface;

final class GenerateFinancialExerciseHandler implements CommandHandlerInterface
{
    public function __construct(
        private PersonalBudgetRepositoryInterface $personalBudgetRepository,
    ) {}

    public function __invoke(GenerateFinancialExerciseCommand $command): FinancialExercise
    {
        $personalBudget = $this->personalBudgetRepository->get($command->personalBudgetId);

        return new FinancialExercise();
    }
}