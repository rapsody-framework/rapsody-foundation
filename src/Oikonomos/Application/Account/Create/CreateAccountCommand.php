<?php

namespace Oikonomos\Application\Account\Create;

use Boulzy\CQRS\CommandInterface;
use Money\Money;

final class CreateAccountCommand implements CommandInterface
{
    public function __construct(
        public readonly string $userId,
        public readonly string $name,
        public readonly ?Money $balance = null,
    ) {}
}