<?php

namespace Oikonomos\Application\Account\Create;

use Boulzy\CQRS\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Oikonomos\Application\Account\Account as AccountDTO;
use Oikonomos\Domain\Model\Account;
use Oikonomos\Domain\Repository\AccountRepositoryInterface;
use Oikonomos\Domain\ValueObject\AccountId;
use Oikonomos\Domain\ValueObject\AccountName;
use Oikonomos\Domain\ValueObject\UserId;
use Oikonomos\Infrastructure\IdGenerator\IdGeneratorInterface;

final class CreateAccountHandler implements CommandHandlerInterface
{
    public function __construct(
        private IdGeneratorInterface $idGenerator,
        private AccountRepositoryInterface $accountRepository,
        private EntityManagerInterface $em,
    ) {}

    public function __invoke(CreateAccountCommand $command): AccountDTO
    {
        $account = Account::create(
            AccountId::fromString($this->idGenerator->generateId()),
            UserId::fromString($command->userId),
            AccountName::fromString($command->name),
            $command->balance
        );

        $this->accountRepository->add($account);

        $this->em->flush();

        return AccountDTO::fromDomain($account);
    }
}