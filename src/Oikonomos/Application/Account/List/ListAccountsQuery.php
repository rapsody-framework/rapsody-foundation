<?php

namespace Oikonomos\Application\Account\List;

use Boulzy\CQRS\QueryInterface;

final class ListAccountsQuery implements QueryInterface
{
    public function __construct(
        public readonly string $userId,
        public readonly ?int $limit = null,
        public readonly ?int $offset = null,
    ) {}
}
