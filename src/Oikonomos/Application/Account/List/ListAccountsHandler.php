<?php

namespace Oikonomos\Application\Account\List;

use Boulzy\CQRS\QueryHandlerInterface;
use Oikonomos\Application\Account\Account;
use Oikonomos\Domain\Repository\AccountRepositoryInterface;
use Oikonomos\Domain\ValueObject\UserId;
use Oikonomos\Infrastructure\Repository\AccountRepository;

final class ListAccountsHandler implements QueryHandlerInterface
{
    public function __construct(
        private AccountRepositoryInterface $accountRepository,
    ) {}

    public function __invoke(ListAccountsQuery $query): iterable
    {
        $accounts = $this->accountRepository->findByUser(
            UserId::fromString($query->userId),
            $query->limit,
            $query->offset
        );

        foreach ($accounts as $account) {
            yield Account::fromDomain($account);
        }
    }
}
