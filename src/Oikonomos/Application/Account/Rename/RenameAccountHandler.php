<?php

namespace Oikonomos\Application\Account\Rename;

use Boulzy\CQRS\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Oikonomos\Application\Account\Account;
use Oikonomos\Domain\Repository\AccountRepositoryInterface;
use Oikonomos\Domain\ValueObject\AccountId;
use Oikonomos\Domain\ValueObject\AccountName;

final class RenameAccountHandler implements CommandHandlerInterface
{
    public function __construct(
        private AccountRepositoryInterface $accountRepository,
        private EntityManagerInterface $em,
    ) {}

    public function __invoke(RenameAccountCommand $command): Account
    {
        $account = $this->accountRepository->get(AccountId::fromString($command->accountId));

        $account->rename(AccountName::fromString($command->name));

        $this->em->flush();

        return Account::fromDomain($account);
    }
}