<?php

namespace Oikonomos\Application\Account\Rename;

use Boulzy\CQRS\CommandInterface;

final class RenameAccountCommand implements CommandInterface
{
    public function __construct(
        public readonly string $accountId,
        public readonly string $name,
    ) {}
}