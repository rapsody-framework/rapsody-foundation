<?php

namespace Oikonomos\Application\Account;

use Money\Money;
use Oikonomos\Domain\Model\Account as AccountEntity;

final class Account
{
    public function __construct(
        public readonly string $id,
        public readonly string $name,
        public readonly Money $balance,
        public readonly bool $closed,
    ) {}

    public static function fromDomain(AccountEntity $entity): Account
    {
        return new self(
            (string) $entity->getId(),
            (string) $entity->getName(),
            $entity->getBalance(),
            $entity->isClosed()
        );
    }
}