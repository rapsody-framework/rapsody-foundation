<?php

namespace Oikonomos\Application\Account\Get;

use Boulzy\CQRS\QueryHandlerInterface;
use Oikonomos\Application\Account\Account;
use Oikonomos\Domain\Repository\AccountRepositoryInterface;
use Oikonomos\Domain\ValueObject\AccountId;

final class GetAccountHandler implements QueryHandlerInterface
{
    public function __construct(
        private AccountRepositoryInterface $accountRepository,
    ) {}

    public function __invoke(GetAccountQuery $query): Account
    {
        $account = $this->accountRepository->get(AccountId::fromString($query->id));

        return Account::fromDomain($account);
    }
}