<?php

namespace Oikonomos\Application\Account\Get;

use Boulzy\CQRS\CommandInterface;

final class GetAccountQuery implements CommandInterface, \Boulzy\CQRS\QueryInterface
{
    public function __construct(
        public readonly string $id,
    ) {}
}