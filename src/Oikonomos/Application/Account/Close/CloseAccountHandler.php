<?php

namespace Oikonomos\Application\Account\Close;

use Boulzy\CQRS\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Oikonomos\Domain\Repository\AccountRepositoryInterface;
use Oikonomos\Domain\ValueObject\AccountId;

final class CloseAccountHandler implements CommandHandlerInterface
{
    public function __construct(
        private AccountRepositoryInterface $accountRepository,
        private EntityManagerInterface $em,
    ) {}

    public function __invoke(CloseAccountCommand $command): void
    {
        $account = $this->accountRepository->get(AccountId::fromString($command->id));

        $account->close($command->force);

        $this->em->flush();
    }
}
