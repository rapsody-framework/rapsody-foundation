<?php

namespace Oikonomos\Application\Account\Close;

use Boulzy\CQRS\CommandInterface;

final class CloseAccountCommand implements CommandInterface
{
    public function __construct(
        public readonly string $id,
        public readonly bool $force = false,
    ) {}
}
