<?php

namespace Oikonomos\Application\User\Create;

use Boulzy\CQRS\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Oikonomos\Domain\Model\User;
use Oikonomos\Domain\Repository\UserRepositoryInterface;
use Oikonomos\Domain\ValueObject\UserId;
use Oikonomos\Infrastructure\IdGenerator\IdGeneratorInterface;

final class CreateUserHandler implements CommandHandlerInterface
{
    public function __construct(
        private IdGeneratorInterface $idGenerator,
        private UserRepositoryInterface $userRepository,
        private EntityManagerInterface $em,
    ) {}

    public function __invoke(CreateUserCommand $command): User
    {
        $user = User::create(
            UserId::fromString($this->idGenerator->generateId())
        );

        $this->userRepository->add($user);

        $this->em->flush();

        return $user;
    }
}