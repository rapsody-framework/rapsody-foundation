<?php

namespace Oikonomos\Application\PersonalBudget\Create;

use Boulzy\CQRS\CommandInterface;

class CreatePersonalBudgetCommand implements CommandInterface
{
    public function __construct(
        public readonly string $userId,
    ) {}
}
