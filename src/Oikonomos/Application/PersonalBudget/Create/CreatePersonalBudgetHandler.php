<?php

namespace Oikonomos\Application\PersonalBudget\Create;

use Boulzy\CQRS\CommandHandlerInterface;
use Oikonomos\Domain\Model\PersonalBudget;
use Oikonomos\Domain\Repository\PersonalBudgetRepositoryInterface;
use Oikonomos\Domain\ValueObject\UserId;

final class CreatePersonalBudgetHandler implements CommandHandlerInterface
{
    public function __construct(
        private PersonalBudgetRepositoryInterface $personalBudgetRepository,
    ) {}

    public function __invoke(CreatePersonalBudgetCommand $command): PersonalBudget
    {
        $personalBudget = PersonalBudget::create(
            $this->personalBudgetRepository->nextIdentity(),
            UserId::fromString($command->userId),
        );

        $this->personalBudgetRepository->add($personalBudget);

        return $personalBudget;
    }
}
