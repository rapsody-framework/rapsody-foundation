<?php

namespace Oikonomos\Application\PersonalBudget\AddIncome;

use Boulzy\CQRS\CommandHandlerInterface;
use Oikonomos\Domain\Model\PersonalBudget;
use Oikonomos\Domain\Repository\PersonalBudgetRepositoryInterface;
use Oikonomos\Domain\ValueObject\IncomeId;
use Oikonomos\Domain\ValueObject\Label;
use Oikonomos\Domain\ValueObject\PersonalBudgetId;
use Oikonomos\Infrastructure\IdGenerator\IdGeneratorInterface;

final class AddIncomeToPersonalBudgetHandler implements CommandHandlerInterface
{
    public function __construct(
        private PersonalBudgetRepositoryInterface $personalBudgetRepository,
        private IdGeneratorInterface $idGenerator,
    ) {}

    public function __invoke(AddIncomeToPersonalBudgetCommand $command): PersonalBudget
    {
        $personalBudget = $this->personalBudgetRepository->get(PersonalBudgetId::fromString($command->personalBudgetId));

        $personalBudget->addIncome(
            IncomeId::fromString($this->idGenerator->generateId()),
            Label::fromString($command->label),
            $command->amount
        );

        return $personalBudget;
    }
}
