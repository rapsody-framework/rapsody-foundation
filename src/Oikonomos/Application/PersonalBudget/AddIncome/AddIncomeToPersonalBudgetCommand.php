<?php

namespace Oikonomos\Application\PersonalBudget\AddIncome;

use Boulzy\CQRS\CommandInterface;
use Money\Money;

class AddIncomeToPersonalBudgetCommand implements CommandInterface
{
    public function __construct(
        public readonly string $personalBudgetId,
        public readonly string $label,
        public readonly Money $amount,
    ) {}
}