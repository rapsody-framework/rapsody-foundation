<?php

namespace Oikonomos\Application\Transaction\Create;

use Boulzy\CQRS\CommandHandlerInterface;
use Boulzy\StaticEventDispatcher\EventDispatcher;
use Boulzy\StaticEventDispatcher\ListenerProvider;
use Doctrine\ORM\EntityManagerInterface;
use Oikonomos\Application\Transaction\Transaction as TransactionDTO;
use Oikonomos\Domain\Listener\AccountBalanceSubscriber;
use Oikonomos\Domain\Model\Transaction;
use Oikonomos\Domain\Repository\AccountRepositoryInterface;
use Oikonomos\Domain\Repository\TransactionRepositoryInterface;
use Oikonomos\Domain\ValueObject\AccountId;
use Oikonomos\Domain\ValueObject\Label;
use Oikonomos\Domain\ValueObject\Tag;
use Oikonomos\Domain\ValueObject\TransactionId;
use Oikonomos\Domain\ValueObject\UserId;
use Oikonomos\Infrastructure\IdGenerator\IdGeneratorInterface;

final class CreateTransactionHandler implements CommandHandlerInterface
{
    public function __construct(
        private IdGeneratorInterface $idGenerator,
        private AccountRepositoryInterface $accountRepository,
        private TransactionRepositoryInterface $transactionRepository,
        private EntityManagerInterface $em,
    ) {}

    public function __invoke(CreateTransactionCommand $command): TransactionDTO
    {
        $account = $this->accountRepository->get(AccountId::fromString($command->accountId));

        if (!$account->getUserId()->equals(UserId::fromString($command->userId))) {
            throw new \RuntimeException('The account does not belong to the user');
        }

        if ($account->isClosed()) {
            throw new \RuntimeException('A new transaction cannot be added to a closed account');
        }

        $listenerProvider = new ListenerProvider();
        $listenerProvider->addSubscriber(new AccountBalanceSubscriber($this->accountRepository));
        EventDispatcher::setListenerProvider($listenerProvider);

        $transaction = Transaction::create(
            TransactionId::fromString($this->idGenerator->generateId()),
            $account->getUserId(),
            $account->getId(),
            Label::fromString($command->label),
            $command->amount,
            $command->date,
            \array_map(fn(string $tag) => Tag::fromString($tag), $command->tags),
        );

        $this->transactionRepository->add($transaction);

        $this->em->flush();

        return TransactionDTO::fromDomain($transaction);
    }
}
