<?php

namespace Oikonomos\Application\Transaction\Create;

use Boulzy\CQRS\CommandInterface;
use Money\Money;

final class CreateTransactionCommand implements CommandInterface
{
    public function __construct(
        public readonly string $userId,
        public readonly string $accountId,
        public readonly string $label,
        public readonly Money $amount,
        public readonly array $tags = [],
        public readonly ?\DateTimeImmutable $date = null,
    ) {}
}
