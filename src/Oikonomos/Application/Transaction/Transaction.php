<?php

namespace Oikonomos\Application\Transaction;

use Money\Money;
use Oikonomos\Domain\Model\Transaction as TransactionEntity;
use Oikonomos\Domain\ValueObject\Tag;

final class Transaction
{
    public function __construct(
        public readonly string $id,
        public readonly string $accountId,
        public readonly string $label,
        public readonly Money $amount,
        public readonly array $tags,
        public readonly \DateTimeImmutable $date,
    ) {}

    public static function fromDomain(TransactionEntity $entity): Transaction
    {
        return new self(
            (string) $entity->getId(),
            (string) $entity->getAccountId(),
            (string) $entity->getLabel(),
            $entity->getAmount(),
            \array_map(fn(Tag $tag) => (string) $tag, $entity->getTags()),
            $entity->getDate(),
        );
    }
}