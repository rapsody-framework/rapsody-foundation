<?php

namespace Oikonomos\Infrastructure\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Oikonomos\Domain\Model\PersonalBudget;
use Oikonomos\Domain\Repository\PersonalBudgetRepositoryInterface;
use Oikonomos\Domain\ValueObject\PersonalBudgetId;

final class PersonalBudgetRepository extends ServiceEntityRepository implements PersonalBudgetRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonalBudget::class);
    }

    public function add(PersonalBudget $personalBudget): void
    {
        $this->_em->persist($personalBudget);
    }

    public function get(PersonalBudgetId $id): PersonalBudget
    {
        if (null === $personalBudget = $this->find((string) $id)) {
            throw new \RuntimeException('Personal Budget not found');
        }

        return $personalBudget;
    }
}
