<?php

namespace Oikonomos\Infrastructure\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Oikonomos\Domain\Model\Transaction;
use Oikonomos\Domain\Repository\TransactionRepositoryInterface;

final class InMemoryTransactionRepository extends ServiceEntityRepository implements TransactionRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    public function add(Transaction $transaction): void
    {
        $this->_em->persist($transaction);
    }
}
