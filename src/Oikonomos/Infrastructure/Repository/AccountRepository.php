<?php

namespace Oikonomos\Infrastructure\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Oikonomos\Domain\Model\Account;
use Oikonomos\Domain\Repository\AccountRepositoryInterface;
use Oikonomos\Domain\ValueObject\AccountId;
use Oikonomos\Domain\ValueObject\AccountStatus;
use Oikonomos\Domain\ValueObject\UserId;

final class AccountRepository extends ServiceEntityRepository implements AccountRepositoryInterface
{
    public const LIST_ALL_ACCOUNTS = 0;
    public const LIST_OPENED_ACCOUNTS = 1;
    public const LIST_CLOSED_ACCOUNTS = 2;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Account::class);
    }

    public function add(Account $account): void
    {
        $this->_em->persist($account);
    }

    public function get(AccountId $id): Account
    {
        if (null === $account = $this->find((string) $id)) {
            throw new \RuntimeException('Account not found');
        }

        return $account;
    }

    public function findByUser(UserId $userId, ?int $limit = null, ?int $offset = null, array $statuses = []): iterable
    {
        $qb = $this->createQueryBuilder('a');
        $qb->where('a.userId.id = :userId');
        $qb->setParameter('userId', (string) $userId);

        if (!empty($statuses)) {
            $qb->andWhere('a.status IN(:statuses)');
            $qb->setParameter('statuses', $statuses);
        }

        if (null !== $limit) {
            $qb->setMaxResults($limit);
        }

        if (null !== $offset) {
            $qb->setFirstResult($offset);
        }

        return $qb->getQuery()->toIterable();
    }
}
