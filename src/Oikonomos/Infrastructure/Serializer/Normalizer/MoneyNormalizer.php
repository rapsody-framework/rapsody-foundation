<?php

namespace Oikonomos\Infrastructure\Serializer\Normalizer;

use Money\Currency;
use Money\Money;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;

final class MoneyNormalizer implements ContextAwareDenormalizerInterface
{
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        if (!\is_array($data)) {
            return false;
        }

        $requiredKeys = ['amount', 'currency'];
        if (0 !== \count(\array_diff_key($data, \array_flip($requiredKeys)))) {
            return false;
        }

        try {
            $this->createMoney($data);
        } catch (\InvalidArgumentException $e) {
            return false;
        }

        return true;
    }

    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        return $this->createMoney($data);
    }

    private function createMoney(array $data): Money
    {
        return new Money($data['amount'], new Currency($data['currency'] ?? 'EUR'));
    }
}
