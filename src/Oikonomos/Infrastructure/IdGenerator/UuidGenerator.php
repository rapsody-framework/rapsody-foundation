<?php

namespace Oikonomos\Infrastructure\IdGenerator;

use Ramsey\Uuid\Uuid;

final class UuidGenerator implements IdGeneratorInterface
{
    public function generateId(): string
    {
        return Uuid::uuid4()->toString();
    }
}
