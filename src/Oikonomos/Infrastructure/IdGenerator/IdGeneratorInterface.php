<?php

namespace Oikonomos\Infrastructure\IdGenerator;

interface IdGeneratorInterface
{
    public function generateId(): string;
}
