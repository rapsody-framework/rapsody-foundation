<?php

namespace Oikonomos\Infrastructure\API\Resource;

use Money\Money;
use Symfony\Component\Serializer\Annotation as Serializer;

final class Transaction
{
    public function __construct(
        #[Serializer\Groups(['get_collection', 'get_item'])]
        public readonly ?string $id,
        #[Serializer\Groups(['get_collection', 'get_item', 'post'])]
        public readonly ?string $accountId,
        #[Serializer\Groups(['get_collection', 'get_item', 'post', 'patch'])]
        public readonly ?string $label,
        #[Serializer\Groups(['get_collection', 'get_item', 'post', 'patch'])]
        public readonly ?Money $amount,
        #[Serializer\Groups(['get_collection', 'get_item', 'post'])]
        public readonly ?array $tags,
        #[Serializer\Groups(['get_collection', 'get_item'])]
        public readonly ?\DateTimeImmutable $date,
    ) {}
}