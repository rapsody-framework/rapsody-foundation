<?php

namespace Oikonomos\Infrastructure\API\Resource;

use Money\Money;
use Symfony\Component\Serializer\Annotation as Serializer;

final class Account
{
    public function __construct(
        #[Serializer\Groups(['get_collection', 'get_item'])]
        public readonly ?string $id,
        #[Serializer\Groups(['get_collection', 'get_item', 'post', 'patch'])]
        public readonly ?string $name,
        #[Serializer\Groups(['get_collection', 'get_item'])]
        public readonly ?Money $balance,
    ) {}
}