<?php

namespace Oikonomos\Infrastructure\API\Resource;

use Money\Money;
use Symfony\Component\Serializer\Annotation as Serializer;

final class User
{
    public function __construct(
        #[Serializer\Groups(['get_collection', 'get_item'])]
        public readonly ?string $id,
        #[Serializer\Groups(['get_collection', 'get_item', 'post', 'put', 'patch'])]
        public readonly ?string $email,
        #[Serializer\Groups(['get_collection', 'get_item'])]
        public readonly ?Money $balance,
    ) {}
}