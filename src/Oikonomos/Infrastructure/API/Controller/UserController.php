<?php

namespace Oikonomos\Infrastructure\API\Controller;

use Boulzy\CQRS\CommandBusInterface;
use Oikonomos\Application\User\Create\CreateUserCommand;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/users')]
final class UserController
{
    public function __construct(
        private CommandBusInterface $commandBus,
    ) {}

    #[Route('', methods: [Request::METHOD_POST])]
    public function create(): Response
    {
        $user = $this->commandBus->dispatch(new CreateUserCommand());

        return new JsonResponse([
            'id' => (string) $user->getId(),
        ]);
    }
}