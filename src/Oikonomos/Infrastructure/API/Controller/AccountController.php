<?php

namespace Oikonomos\Infrastructure\API\Controller;

use Boulzy\CQRS\CommandBusInterface;
use Boulzy\CQRS\QueryBusInterface;
use Oikonomos\Application\Account\Close\CloseAccountCommand;
use Oikonomos\Application\Account\Create\CreateAccountCommand;
use Oikonomos\Application\Account\Get\GetAccountQuery;
use Oikonomos\Application\Account\List\ListAccountsQuery;
use Oikonomos\Application\Account\Rename\RenameAccountCommand;
use Oikonomos\Infrastructure\API\Resource\Account;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

#[Route(path: '/accounts')]
final class AccountController
{
    public function __construct(
        private Security $security,
        private CommandBusInterface $commandBus,
        private QueryBusInterface $queryBus,
    ) {}

    #[Route('', methods: Request::METHOD_POST)]
    public function create(Account $account): Account
    {
        $user = $this->security->getUser();
        if (null === $user) {
            throw new \RuntimeException('No user logged in');
        }

        /** @var \Oikonomos\Application\Account\Account $accountDTO */
        $accountDTO = $this->commandBus->dispatch(new CreateAccountCommand(
            $user->getUserIdentifier(),
            $account->name,
            $account->balance,
        ));

        return new Account(
            $accountDTO->id,
            $accountDTO->name,
            $accountDTO->balance,
        );
    }

    #[Route('', methods: Request::METHOD_GET)]
    public function list(Request $request): array
    {
        $user = $this->security->getUser();
        if (null === $user) {
            throw new \RuntimeException('No user logged in');
        }

        $accountsDTO = $this->queryBus->dispatch(new ListAccountsQuery(
            $user->getUserIdentifier(),
            $request->query->get('limit', 10),
            $request->query->get('offset', 0)
        ));

        $accounts = [];
        foreach ($accountsDTO as $accountDTO) {
            $accounts[] = new Account(
                $accountDTO->id,
                $accountDTO->name,
                $accountDTO->balance,
            );
        }

        return $accounts;
    }

    #[Route('/{id}', methods: Request::METHOD_GET)]
    public function get(string $id): Account
    {
        $account = $this->queryBus->dispatch(new GetAccountQuery($id));

        return new Account(
            $account->id,
            $account->name,
            $account->balance,
        );
    }

    #[Route('/{id}', methods: Request::METHOD_PATCH)]
    public function rename(string $id, Account $account): Account
    {
        $account = $this->commandBus->dispatch(new RenameAccountCommand($id, $account->name));

        return new Account(
            $account->id,
            $account->name,
            $account->balance,
        );
    }

    #[Route('/{id}', methods: Request::METHOD_DELETE)]
    public function close(string $id): void
    {
        $this->commandBus->dispatch(new CloseAccountCommand($id));
    }
}
