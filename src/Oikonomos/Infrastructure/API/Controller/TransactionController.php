<?php

namespace Oikonomos\Infrastructure\API\Controller;

use Boulzy\CQRS\CommandBusInterface;
use Oikonomos\Application\Transaction\Create\CreateTransactionCommand;
use Oikonomos\Domain\ValueObject\Tag;
use Oikonomos\Infrastructure\API\Resource\Transaction;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

#[Route('/transactions')]
final class TransactionController
{
    public function __construct(
        private Security $security,
        private CommandBusInterface $commandBus,
    ) {}

    #[Route('')]
    public function create(Transaction $transaction): Transaction
    {
        $user = $this->security->getUser();
        if (null === $user) {
            throw new \RuntimeException('No user logged in');
        }

        /** @var \Oikonomos\Application\Transaction\Transaction $transactionDTO */
        $transactionDTO = $this->commandBus->dispatch(new CreateTransactionCommand(
            $user->getUserIdentifier(),
            $transaction->accountId,
            $transaction->label,
            $transaction->amount,
            $transaction->tags,
            $transaction->date,
        ));

        return new Transaction(
            $transactionDTO->id,
            $transactionDTO->accountId,
            $transactionDTO->label,
            $transactionDTO->amount,
            $transactionDTO->tags,
            $transactionDTO->date,
        );
    }
}
