<?php

namespace Tests\Rapsody;

use PHPUnit\Framework\TestCase;
use Rapsody\Framework\RapsodyKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Symfony\Component\Routing\RequestContext;

final class FrameworkTest extends TestCase
{
    private UrlMatcherInterface $matcher;
    private ControllerResolverInterface $controllerResolver;
    private ArgumentResolverInterface $argumentResolver;
    private RapsodyKernel $framework;

    public function setUp(): void
    {
        $this->matcher = \Mockery::mock(UrlMatcherInterface::class);
        $this->matcher->expects()->getContext()->zeroOrMoreTimes()->andReturn(new RequestContext());

        $this->controllerResolver = \Mockery::mock(ControllerResolverInterface::class);

        $this->argumentResolver = \Mockery::mock(ArgumentResolverInterface::class);

        $this->framework = new RapsodyKernel($this->matcher, $this->controllerResolver, $this->argumentResolver);
    }

    public function testRouting(): void
    {
        $content = 'OK';

        $this->matcher->expects()->match()->once()->withArgs(['/'])->andReturn([
            '_route' => 'test',
            '_controller' => 'callable',
        ]);

        $this->controllerResolver->expects()->getController()->once()->withAnyArgs()->andReturn(
            function () use ($content): Response {
                return new Response($content);
            }
        );

        $this->argumentResolver->expects()->getArguments()->once()->withAnyArgs()->andReturn([]);

        $response = $this->framework->handle(new Request());

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($content, $response->getContent());
    }

    public function testNotFound(): void
    {
        $this->matcher->expects()->match()->once()->withArgs(['/'])->andThrows(ResourceNotFoundException::class);

        $response = $this->framework->handle(new Request());

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testInternalServerError(): void
    {
        $this->matcher->expects()->match()->once()->withArgs(['/'])->andThrows(\RuntimeException::class);

        $response = $this->framework->handle(new Request());

        $this->assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
    }
}
