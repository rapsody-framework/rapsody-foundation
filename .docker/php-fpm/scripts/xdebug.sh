#!/bin/ash
set -e

: ${XDEBUG:=0}
#: ${XDEBUG_KEY:=phpstorm}
#: ${XDEBUG_PORT_REMOTE:=9000}
#: ${XDEBUG_HOST_REMOTE:=172.17.0.2}

file="/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini"

rm -f $file

if [ "$XDEBUG" = 1 ]; then
    echo "[xdebug]" >> $file
    echo "zend_extension=xdebug" >> $file
    echo "Xdebug enabled"
#    echo "xdebug.max_nesting_level=5000" >> $file
#    echo "xdebug.discover_client_host = 1" >> $file
#    echo "xdebug.mode = debug" >> $file
#    echo "xdebug.client_port = $XDEBUG_PORT_REMOTE" >> $file
#    echo "xdebug.idekey = '$XDEBUG_KEY'" >> $file
#    echo "xdebug.client_host = '$XDEBUG_HOST_REMOTE'" >> $file
#    echo "xdebug.start_with_request = yes" >> $file
fi

# first arg is `-f` or `--some-option`
if [ "" = "$1" ] || [ "${1#-}" != "$1" ]; then
    if [ -x "$(command -v php-fpm)" ]; then
        set -- php-fpm "$@"
    else
        set -- php "$@"
    fi
fi

exec "$@"
