<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220518102229 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE Account (id VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, userid VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, balance_amount VARCHAR(255) NOT NULL, balance_currency_code VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE PersonalBudget (id VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE Transaction (id VARCHAR(255) NOT NULL, tags JSON NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, userid VARCHAR(255) NOT NULL, accountid VARCHAR(255) NOT NULL, label VARCHAR(255) NOT NULL, transactionamount VARCHAR(255) NOT NULL, transactioncurrency_code VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN Transaction.date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE "User" (id VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE Account');
        $this->addSql('DROP TABLE PersonalBudget');
        $this->addSql('DROP TABLE Transaction');
        $this->addSql('DROP TABLE "User"');
    }
}
